# mentorama-java
import java.util.Scanner;

public class Menu {
    private static int opcao = 0;

    public static void main(String[] args) {
        
        while (opcao!=3) {
        System.out.println("|    MENU                |");
        System.out.println("|  Opções:               |");
        System.out.println("|         1. Opção 1     |");
        System.out.println("|         2. Opção 2     |");   
        System.out.println("|         3. Sair        |");

        System.out.println(" Digite a opção desejada: "); 

        Scanner menu = new Scanner(System.in);
        opcao = menu.nextInt();

        switch (opcao) {
        case 1:
            System.out.println("Você escolheu a primeira opção");
            break;

        case 2:
            System.out.println("Você escolheu a segunda opção");
            break;

        case 3:
            System.out.println("O programa foi encerrado");
            break;

        default:
            System.out.println("Seleção inválida");
            break;    
        }
    }
        }
}
